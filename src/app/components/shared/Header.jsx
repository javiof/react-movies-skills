import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { Typography } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import {Link} from "react-router-dom";


const useStyles = makeStyles(theme => ({
  root: {
    background: '#f44336',
  },
}));
export default function Header(props) {
  const classes = useStyles();
 
  return (
    <React.Fragment>
      <AppBar  className={classes.root}>
        <Toolbar>
          <Container>
            <Grid container spacing={3}> 
              <Grid item xs={9} sm={9}>
              <Link to='/' className='title'><Typography variant="h6">React Movies</Typography></Link>
              </Grid>
              <Grid item xs={3} sm={3}>
                
              </Grid>
            </Grid>
          </Container>
        </Toolbar>
      </AppBar>
      <Toolbar id="back-to-top-anchor" />
    </React.Fragment>
  );
}




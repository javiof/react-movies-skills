import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';


const useStyles = makeStyles(theme => ({
  root: {    
    marginTop: 10,
    display: 'flex',
    alignItems: 'center',  
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  }  
}));

export default function Search({ searchMovies }) {
  const classes = useStyles();

  return (
    <Paper className={classes.root}>   
      <InputBase
        className={classes.input}
        placeholder="Search" 
        onChange={searchMovies}     
      />
      <IconButton className={classes.iconButton} aria-label="search">
        <SearchIcon />
      </IconButton>
     
    </Paper>
  );
}
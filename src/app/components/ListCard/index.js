import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import RecipeReviewCard from '../RecipeReviewCard';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: 40
  },
  paper: {  
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));



export default function ListCard(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid container item xs={12} spacing={2}>
        {props.movies.map(movie => (
          <Grid item xs={12} sm={6} lg={3} key={movie.id}>
            <RecipeReviewCard movie={movie}/>
            </Grid>    
        ))}
          </Grid>    
    </Grid>
    </div>
  );
}
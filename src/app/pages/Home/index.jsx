import React, { Component } from 'react';
import axios from 'axios';
import InfiniteScroll from 'react-infinite-scroll-component';
import { api_key } from '../../utils/apiKey';
import Search from '../../components/Search';
import NavTabs from '../../components/NavTabs';
import Grid from '@material-ui/core/Grid';
import ListCard from '../../components/ListCard';
import Container from '@material-ui/core/Container';

import './style.css';



export default class Home extends Component {

  
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
      genres: [],
      actualPage: 2,
      actualFilter: '',
      fetchMore: true,
      totalPages: 0,
      actualState: '',
      movieSearching: ''
    };      
}


  
  componentDidMount() {
    axios.all([this.getMovies(), this.getGenres()]).then(data => {
      let movies = data[0].data.results.map(movie => {
        let genres = [];
        for (let genreId of movie.genre_ids) {
          genres.push(
            data[1].data.genres.find(genre => genre.id === genreId).name
          );
        }
        movie.genres = genres.join(', ');

        return movie;
      });
      this.setState({
        movies,
        genres: data[1].data.genres
      });
    });
  }

  getMovies(category, page = 1) {
    switch (category) {
      case 'Popular':
        return axios.get(
          `https://api.themoviedb.org/3/movie/popular?api_key=${api_key}&page=${page}`
        );
      case 'Upcoming':
        return axios.get(
          `https://api.themoviedb.org/3/movie/upcoming?api_key=${api_key}&page=${page}`
        );
      default:
        return axios.get(
          `https://api.themoviedb.org/3/trending/movie/week?api_key=${api_key}&page=${page}`
        );
    }
  }

  getGenres() {
    return axios.get(
      `https://api.themoviedb.org/3/genre/movie/list?api_key=${api_key}&language=en-US`
    );
  }

  fetchMoreData = () => {
    if (this.state.actualState === 'searching-movies') {
      if (this.state.actualPage >= this.totalPages) {
        this.setState({ fetchMore: false });
        return;
      }
      axios
        .get(
          `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&query=${
            this.state.movieSearching
          }&page=${this.state.actualPage}`
        )
        .then(res => {
          let movies = res.data.results.map(movie => {
            let genres = [];
            for (let genreId of movie.genre_ids) {
              genres.push(
                this.state.genres.find(genre => genre.id === genreId).name
              );
            }
            movie.genres = genres.join(', ');

            return movie;
          });
          this.setState({
            movies: this.state.movies.concat(movies),
            actualPage: this.state.actualPage + 1
          });
        });
    } else {
      axios
        .all([this.getMovies(this.state.actualFilter, this.state.actualPage)])
        .then(data => {
          let movies = data[0].data.results.map(movie => {
            let genres = [];
            for (let genreId of movie.genre_ids) {
              genres.push(
                this.state.genres.find(genre => genre.id === genreId).name
              );
            }
            movie.genres = genres.join(', ');

            return movie;
          });

          this.setState({
            movies: this.state.movies.concat(movies),
            actualPage: this.state.actualPage + 1
          });
        });
    }
  };

  handleFilter = filterName => {
    axios.all([this.getMovies(filterName, 1)]).then(data => {
      let movies = data[0].data.results.map(movie => {
        let genres = [];
        for (let genreId of movie.genre_ids) {
          genres.push(
            this.state.genres.find(genre => genre.id === genreId).name
          );
        }
        movie.genres = genres.join(', ');

        return movie;
      });
      this.setState({ movies, actualFilter: filterName });
    });
  };

  searchMovies = e => {
    let movieName = e.target.value;
    if (movieName === '') {
      axios.all([this.getMovies()]).then(data => {
        let movies = data[0].data.results.map(movie => {
          let genres = [];
          for (let genreId of movie.genre_ids) {
            genres.push(
              this.state.genres.find(genre => genre.id === genreId).name
            );
          }
          movie.genres = genres.join(', ');

          return movie;
        });
        this.setState({ movies, actualState: '', movieSearching: '' });
      });
      return;
    }
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=${api_key}&query=${movieName}&page=1`
      )
      .then(res => {
        let movies = res.data.results.map(movie => {
          let genres = [];
          for (let genreId of movie.genre_ids) {
            genres.push(
              this.state.genres.find(genre => genre.id === genreId).name
            );
          }
          movie.genres = genres.join(', ');

          return movie;
        });
        this.setState({
          movies,
          totalPages: res.data.total_pages,
          actualState: 'searching-movies',
          movieSearching: movieName
        });
      });
  };

  render() {   
    return (
      <Container className="home_container">        
        <Grid container spacing={1}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={9} >
                  <NavTabs handleFilter={this.handleFilter} />
                </Grid>    
              <Grid item xs={3} >
                <Search searchMovies={this.searchMovies} />
              </Grid>    
            </Grid>    
        </Grid>
        <Grid container spacing={1}>
          <InfiniteScroll
            dataLength={this.state.movies ? this.state.movies.length : 0}
            next={this.fetchMoreData}
            hasMore={this.state.fetchMore}
          >
           <ListCard movies={this.state.movies} />
          </InfiniteScroll> 
        </Grid>       
      </Container>
    );
  }
}

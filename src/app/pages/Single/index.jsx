import React, { Component } from 'react';
import { withStyles } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import Container from '@material-ui/core/Container';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import SlowMotionVideoIcon from '@material-ui/icons/SlowMotionVideo';
import Button from '@material-ui/core/Button';
import axios from 'axios';
import moment from 'moment';
import { api_key } from '../../utils/apiKey';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: 20,
    margin: 'auto',
    maxWidth: '100%',
  },
  image: {
    width: '100%',
    height: '100%',
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
  button: {
    background: '#f44336',
  },
});

class Single extends Component {
  state = {
    movie: {},
    genres: [],
    trailerCode: ''
  };

  componentDidMount() {
    axios.all([this.getMovieInfo(), this.getMovieTrailer()]).then(res => {
      this.setState({
        movie: res[0].data,
        genres: res[0].data.genres,
        trailerCode: res[1].data.results[0].key
      });
    });
  }

  getMovieInfo() {
    return axios.get(
      `https://api.themoviedb.org/3/movie/${
        this.props.match.params.id
      }?api_key=${api_key}`
    );
  }

  getMovieTrailer() {
    return axios.get(
      `https://api.themoviedb.org/3/movie/${
        this.props.match.params.id
      }/videos?api_key=${api_key}`
    );
  }

  
  render() {    

    const { classes } = this.props;

    const {     
      original_title,
      release_date,
      overview,
      poster_path
    } = this.state.movie;
    const { genres, trailerCode } = this.state;
    return (

      <Container className="home_container">   
      <div className={classes.root}>
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <Grid item>
            <ButtonBase className={classes.image}>
              {poster_path?
              <img className={classes.img} alt='complex' src={'https://image.tmdb.org/t/p/w500/' + poster_path} />
              :null}
            </ButtonBase>
          </Grid>
          <Grid item xs={12} sm container>
            <Grid item xs container direction='column' spacing={2}>
              <Grid item xs>
                <Typography gutterBottom variant='h3'>
                 {original_title}
                </Typography>
                <Typography variant='body2' gutterBottom>
                 {overview}
                </Typography>                
                <Typography variant='body2' color='textSecondary'>
                    <LocalOfferIcon />                    
                    {genres.map((genre, i) => (
                          <span key={i}>                            
                            {genre.name}
                            {i + 1 < genres.length && ', '}
                          </span>
                    ))}                                    
                </Typography>
              </Grid>
              <Grid item xs>
                <Typography variant='h2' style={{ cursor: 'pointer' }}>
                  <Button
                    variant='contained'
                    color='secondary'                        
                    href={'https://www.youtube.com/watch?v=' + trailerCode}
                    target="_blank"
                    className={classes.button}                                               
                  >
                      {'Trailer'}&nbsp;<SlowMotionVideoIcon />
                  </Button>
                </Typography>
              </Grid>
            </Grid>
            <Grid item>
              <Typography variant='subtitle1'> {moment(release_date).format('MM/DD/YYYY')}</Typography>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </div>
    </Container>
  );
  }
}

export default withStyles(styles)(Single);
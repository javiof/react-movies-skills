import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline'
import Layout from './app/Layout';

import { BrowserRouter as Router } from 'react-router-dom';


function App() {
  return (
    <Router>  
      <React.Fragment>
        <CssBaseline />    
        <Layout />
      </React.Fragment>
    </Router>   
    
  );
}

export default App;
